package com.assignmentDatadrivenTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class RegisterUsingDDT {

	public static void main(String[] args) throws BiffException, IOException {
		File f=new File("/home/Saikumar/Documents/Test_data/Testdata2.xls");
		FileInputStream fis=new FileInputStream(f);
		Workbook b=Workbook.getWorkbook(fis);
		Sheet s=b.getSheet("Registerdata");



		int rows= s.getRows();
		int columns=s.getColumns();
		for(int i=1;i<rows;i++) {
			String firstname=s.getCell(0, i).getContents();
			String lastname=s.getCell(1, i).getContents();
			String email=s.getCell(2, i).getContents();
			String password=s.getCell(3, i).getContents();


			WebDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("https://demowebshop.tricentis.com/register");
			driver.findElement(By.id("gender-male")).click();

			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
            driver.findElement(By.id("register-button")).click();
		}

	}}
